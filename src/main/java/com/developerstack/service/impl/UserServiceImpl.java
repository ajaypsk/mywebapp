/**
 * 
 */
package com.developerstack.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.developerstack.model.UserDetails;
import com.developerstack.service.UserService;

@Component
public class UserServiceImpl implements UserService {
	
	public UserDetails validateUser(Map<String, String> user) {
		String email = user.get("email");
		String password = user.get("password");
		
			if(!"test".equals(password)) {
			throw new RuntimeException("Invalid credentials.");
		}
		UserDetails	userDetails = new UserDetails(); 
		userDetails.setEmail(email);
		userDetails.setPassword(password);
		userDetails.setFirstName("test");
		userDetails.setLastName("test");
		return userDetails;
	}

	public List<UserDetails> getUserDetails() {
		
		 List<UserDetails> lstDetails = new ArrayList<UserDetails>();
		
		UserDetails	userDetails = new UserDetails(); 
		userDetails.setEmail("test@test.com");
		userDetails.setPassword("test");
		userDetails.setFirstName("test");
		userDetails.setLastName("test");
		lstDetails.add(userDetails);
		return lstDetails;

	}

}
